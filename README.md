Задание:
1) Открыть https://auth-demo.aerobatic.io/protected-standard/
2) Нажать кнопку ""Standart auth""
3) Пройти авторизацию 

Уточню по поводу практики, на Selenium достаточно выполнить третий шаг. Не забудьте добавить ожидаемый результат (проверку, что мы реально авторизовались). 
Т.е. в тесте можно сразу открывать url https://auth-demo.aerobatic.io/protected-standard/, без промежуточных нажиманий на "Standart auth"
     
Запуск теста:
1) В файле \src\test\resources\config.properties заполнить проперти соответствующими значениями:  
aerobatic.io.login - логин пользователя
aerobatic.io.password - пароль пользователя 	 

2) mvn clean -Dtest=BasicAuthCase test