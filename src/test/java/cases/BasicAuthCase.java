package cases;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.proxy.auth.AuthType;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.core.io.ClassPathResource;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.BasicAuthPage;

import java.io.IOException;
import java.util.Properties;

import static org.testng.Assert.fail;

public class BasicAuthCase {

    private final String BASIC_AUTH_PAGE_URL = "http://auth-demo.aerobatic.io/protected-standard/";
    private RemoteWebDriver driver;
    private BrowserMobProxyServer bmp;
    private String login = "";
    private String password = "";
    private BasicAuthPage basicAuthPage;

    @BeforeClass
    public void setUp() throws Exception {
        initProperties();
        ChromeDriverManager.getInstance().version("2.29").setup();
        bmp = new BrowserMobProxyServer();
        bmp.start();

        bmp.autoAuthorization("auth-demo.aerobatic.io", login, password, AuthType.BASIC);
        Proxy proxy = ClientUtil.createSeleniumProxy(bmp);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.PROXY, proxy);
        driver = new ChromeDriver(capabilities);
    }

    @Test
    public void testBasicAuth() throws Exception {
        driver.get(BASIC_AUTH_PAGE_URL);
        basicAuthPage = new BasicAuthPage(driver);
        Assert.assertTrue(
                basicAuthPage.getTitle().getText().
                        contains("Auth Success"));
    }

    private void initProperties() throws IOException {
        Properties config = new Properties();
        try {
            config.load(new ClassPathResource("config.properties").getInputStream());
            login = config.getProperty("aerobatic.io.login");
            password = config.getProperty("aerobatic.io.password");
        } catch (IOException ex) {
            ex.printStackTrace();
            fail("File 'config.properties' can't be open!");
        }
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.close();
        driver.quit();
        bmp.stop();
    }
}
