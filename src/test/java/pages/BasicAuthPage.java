package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class BasicAuthPage extends BasePage {
    @FindBy(xpath = "//h1[@class='title']")
    WebElement title;

    public BasicAuthPage(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getTitle() {
        return getElement(title);
    }
}
